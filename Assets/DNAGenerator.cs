﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
[ExecuteInEditMode]
public class DNAGenerator : MonoBehaviour {
    private LineRenderer strand;
    public int numVertices = 20;
    public bool invertYZ = false;
    public float scale = .5f;
    private float _scale;
    private Vector3[] strandVerts;
    private Vector3 dnaPos;
	// Use this for initialization
	void Start () {
        strand = GetComponent<LineRenderer>();
        CreateStrand();
        SetStrand();
    }

    // Update is called once per frame
    void Update()
    {
        if (strand.numPositions != numVertices)
        {
            CreateStrand();
            SetStrand();
        }
        if(scale !=_scale || transform.position != dnaPos)
        {
            SetStrand();
        }

    }
    void CreateStrand()
    {
        strandVerts = new Vector3[numVertices];
        strand.numPositions = numVertices;
    }
    void SetStrand()
    {
        _scale = scale;
        dnaPos = transform.position;
        for (int i = 0; i < numVertices; ++i)
        {
            float t = i * .2f;
            strandVerts[i] = new Vector3(t * 1.5f, (invertYZ ? 1 : -1) * 2 * Mathf.Sin(t), 2 * (invertYZ ? 1 : -1) * Mathf.Cos(t)) * _scale + dnaPos;
        }
        strand.SetPositions(strandVerts);
    }
}
