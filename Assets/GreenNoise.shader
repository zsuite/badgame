﻿Shader "Unlit/GreenNoise"
{
	Properties
	{
		_MainColor("Color", Color) = (0, 0, 0, 1)
		_WireColor("Wire Color",Color) = (0,1,0,1)
		_Width("Wire Width", Range(0,.5)) = 0.455
	}
		SubShader
	{
		Tags { "RenderType" = "Transparent" }
		ZWrite ON
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			uniform float4 _MainColor;
			uniform float4 _WireColor;

			float4 _MainTex_ST;
			uniform float _Width;
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col;
			
				float closeness = step(_Width, abs(i.uv.x - .5)) || step(_Width, abs(i.uv.y - .5));
				col = lerp(_MainColor, _WireColor, closeness*_WireColor.a);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
