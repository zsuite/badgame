﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CycleNucleotide : MonoBehaviour {

    char[] nucleotides = { 'A', 'T', 'C', 'G' };
    int i = 0;
    public void Cycle()
    {
        
        GetComponentInChildren<Text>().text = nucleotides[i].ToString();
        if (i == 3)
        { i = 0; }
        ++i;
    }
}
