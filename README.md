# README #

hacking4cyborgs

Hacking 4 cyborgs is an educational game with the purpose of teaching the players the proper arrangement of nucleotides in DNA. 
My goal was to make a game that turns friendly gameplay atypical of mainstream games, into a malicious ransomware attack on a computer. 
I wanted to explore an increasingly common issue many people face when they accidentally share ransomware or malware with their friends. 
In this game, the player can choose to commit a malicious hack in order to learn whether they chose the correct nucleotide base pairs, 
or they could decide that being a non malicious person is worth more than knowing whether they were right or wrong. 
I wanted to have the player hack a computer without feeling empowered, and without feeling in control of the machine. 
Basically to take hacking, a traditionally masculine and dominating act, to something more appealing to players that do not crave power. 


This project requires a Rubber Ducky USB device.
When the Rubber Ducky is in the target PC, the player will be able to watch as commands are being sent through the computer. 
This Bad Game is a game that is evil in its nature. 
The winning scenario has all of the files of an unsuspected PC encrypted. 
The losing scenario only prints a friendly message to the screen indicating your nucleotide pairs are wrong.
